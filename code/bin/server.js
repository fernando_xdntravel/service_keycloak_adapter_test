const App = require("../app");

const Server = class extends App {
    constructor() {
        super();
        this.init();
    }

    init() {
        const port = this.normalize(process.env.PORT || "3000");
        this.app.listen(port, function () {
            console.info(`server running in port ${port}`);
        });
    }

    normalize(value) {
        const port = parseInt(value, 10);
        if (isNaN(port)) {
            return value;
        }
        if (port >= 0) {
            return port;
        }
        return false;
    }
};

module.exports = new Server();
