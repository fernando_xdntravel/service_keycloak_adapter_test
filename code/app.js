const express = require("express");
const morgan = require("morgan");
const cors = require("cors");

const { stream } = require("./src/utils");
const router = require("./src/routes");

module.exports = class App {
    constructor() {
        this.app = express();
        this.env = process.env.NODE_ENV || "development";
        this.initializeMiddlewares();
        this.initializeRoutes();
    }

    initializeMiddlewares() {
        if (this.env === "production") {
            this.app.use(morgan("combined", { stream }));
            this.app.use(cors({ origin: "*", credentials: true }));
        } else if (this.env === "development") {
            this.app.use(morgan("dev", { stream }));
            this.app.use(cors({ origin: true, credentials: true }));
        }
    }

    initializeRoutes() {
        this.app.use(router);
    }
};
