const winston = require("winston");

//winston formart
const { printf } = winston.format;

const loggerFormat = printf(({ level, message }) => `${level}: ${message} `);

const logger = winston.createLogger({
    format: loggerFormat,
    transports: [
        new winston.transports.File({
            dirname: "log",
            filename: "error.log",
            level: "error",
        }),
        new winston.transports.File({
            dirname: "log",
            filename: "combined.log",
        }),
        new winston.transports.Console({
            format: winston.format.combine(
                winston.format.splat(),
                winston.format.colorize(),
                winston.format.simple()
            ),
        }),
    ],
});

const stream = {
    write(message) {
        logger.info(message.substring(0, message.lastIndexOf("\n")));
    },
};

module.exports = {
    logger,
    stream,
};
