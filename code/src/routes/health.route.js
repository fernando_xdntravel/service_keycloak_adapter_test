const { Router } = require("express");

class Health {
    constructor() {
        this.route = Router();
        this.createRoutes();
    }

    createRoutes() {
        //check health server
        this.route.get("/", (req, res) => {
            res.json({
                server: "UP",
            });
        });
    }
}

module.exports = new Health().route;
