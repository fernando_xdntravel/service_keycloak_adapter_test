const { Router } = require("express");
const health = require("../routes/health.route");
const credentials = require("../routes/credentials.route");

const prefix = `/api`;
const router = Router();

router.use(`${prefix}/`, health);
router.use(`${prefix}/`, credentials);

module.exports = router;
