const { Router } = require("express");
const Credentials = require("../controllers/credentials.controller");

class Health {
    constructor() {
        this.route = Router();
        this.createRoutes();
    }

    createRoutes() {
        //check keycloak health
        this.route.get("/check", (req, res, next) =>
            Credentials.checkHealth(req, res, next)
        );
    }
}

module.exports = new Health().route;
