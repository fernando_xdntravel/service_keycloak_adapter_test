FROM node:12-slim

# Create app directory
ENV dir_base '/var/www/code'

RUN apt-get update

ENV RELEASE 1.0.0
RUN mkdir -p ${dir_base}
ADD code/ "$dir_base/"

WORKDIR ${dir_base}
RUN npm install

ENTRYPOINT [ "node","bin/server.js" ]
